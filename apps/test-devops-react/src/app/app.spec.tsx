import { render } from '@testing-library/react';
import { mount } from 'enzyme';
import App from './app';


describe('App', () => {
  it('should render successfully', () => {
    const { baseElement } = render(<App />);

    expect(baseElement).toBeTruthy();
  });

  it('should have a greeting as the title', () => {
    const { getByText } = render(<App />);

    expect(getByText(/Welcome test-devops-react/gi)).toBeTruthy();
  });

  it('should show a one of all badge status', () => {
    const wrapper = mount(<App/>);
    const buttonReloadUser = wrapper.find('#button-reload-user');
    const badgeStatusPending = wrapper.find('#bbadge-status-pending');
    buttonReloadUser.simulate('click');
    expect(badgeStatusPending.exists()).toBeTruthy()
  })
});
