// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { useEffect, useState } from 'react';
import { environment } from '../environments/environment';

const contentStyle = {
  display: 'block',
  alignItems: 'center',
}

const reloadUserButtonStyle = {
  border: 'none',
  backgroundColor: '#333',
  color: '#FFF',
  padding: '15px 10px',
  cursor: 'pointer',
  marginBottom: 15
}

const textLabelStyle = {
  fontFamily: 'monospace',
  fontSize: 16
}

const loaderStyle = {
  fontFamily: 'monospace',
  fontSize: 16,
  opacity: 0.5
}

export function App() {

  const { environment: environmentName, NX_API_URL } = environment

  const getNxApiEndpoint = () => {
    if (environmentName === 'develop') {
      return 'animal'
    }

    if (environmentName === 'staging') {
      return 'number'
    }

    if (environmentName === 'production') {
      return 'city'
    }

    return ''
  }
  

  const [userResponse, setUserResponse] = useState<any>({
    pending: false,
    hasError: false,
    response: ''
  })

  const getRandomUser = () => {
    setUserResponse({
      pending: true,
      hasError: false,
      response: ''
    })
    fetch(`${NX_API_URL}${getNxApiEndpoint()}`)
      .then((res) => res.json())
      .then((res) => setUserResponse({
        pending: false,
        hasError: false,
        response: res
      }))
      .catch((err) => {
        setUserResponse({
          pending: false,
          hasError: true,
          response: err.message
        })
      })
  }

  useEffect(() => {
    getRandomUser()
  }, [])
  
  const UserJsonResponse = (response: any) => (
    <pre>
      {JSON.stringify(response, null, 3)}
      </pre>
  )

  const Loader = () => (
    <p style={loaderStyle}>Cargando...</p>
  )

  const ReloadUserButton = () => (
    <button 
      id='button-reload-user'  
      style={reloadUserButtonStyle} 
      onClick={getRandomUser}>
      Cargar nuevo usuario
    </button>
  )

  return (
    <div style={{...contentStyle, flexDirection: 'column'}}>
      <h2 style={{...textLabelStyle, fontSize: 20}}>Test Devops Serempre</h2>
      <p style={textLabelStyle}>Enviroment: {environmentName}</p>
      <p style={{...textLabelStyle, display: 'flex', alignItems: 'center'}}><span style={{marginRight: 5}}>Status </span>  {
        userResponse.pending ? (
          <div id='badge-status-pending' style={{width: 15, height: 15, borderRadius: '50%', backgroundColor: 'skyblue'}}></div>
          ) : !userResponse.hasError ? (
          <div id='badge-status-ok' style={{width: 15, height: 15, borderRadius: '50%', backgroundColor: 'green'}}></div>
          ) : (
            <div id='badge-status-error' style={{width: 15, height: 15, borderRadius: '50%', backgroundColor: 'red'}}></div>
            )
          }</p>
          <ReloadUserButton/>
          <hr/>
        {
          userResponse.response ?  <UserJsonResponse response={userResponse.response} /> : <Loader/>
        }
      </div>
  );
}

export default App;
